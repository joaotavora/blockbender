# A MOBILITY MARKET BACKED BY SMART CONTRACTS
### The programators - Team 5

# Who we are

João and Luís have been software engineers for ~10 years and work in resource scheduling optimisation software for the railway domain. David and Anbang are engineering students at IST. We're all Blockchain newbies.

# Idea

**TL;DR:** A Blockchain for mobility resources such as parking spots, toll roads, car/bike/scooter sharing, ride-sharing, EV charging stations, public transport establishing a market backed by smart contracts.

## Overview / Sketch

  1. **Resource providers publish smart contracts** on a regular basis. E.g.: one such contract might state "for the next year, parking on street _x_ will cost y €/h", or "for the next 24h, diesel at gas station _x_ will cost _y_ €/l", and so on.

  2. **Certification of resource providers** is backed by smart contracts in hierarchical fashion. Think DNS. A [IANA](https://en.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority)-like authority would control a root Smart Contract and delegate certification permissions down to countries, cities, companies, etc. (E.g. Root => Portugal => Lisbon => EMEL => parking spot down the road, or Root => Spain => GALP => random gas station)

  3. **Resource consumer certification.** In some cases, resource consumers might need (or benefit from) certification as well. E.g., I don't like giving away my personal details to each and every car sharing company. If I could be certified by my government and have the providers trust that certification that'd be more convenient. HERE BE PRIVACY DRAGONS, though.
     
  4. **Search services can index these smart contracts** and present them in useful ways. Think Google Maps, Waze, etc. The smart contract might include a fee for the search provider when the consumer pays for the resource.
     
  5. **Resource consumers consume and pay for the resource**. Mediation between search engine, provider, consumer and some third-party verifier that measures consumption (e.g. an actual physical gas pump) by the smart contract. HERE BE PRIVACY DRAGONS (again); is it a problem that transactions will be public and traceable back to the resource provider's location?
  
  6. **Resource quality can be monitored by consumers** (traffic data, gas availability, bike maintenance status, etc) but I'm not yet sure what the incentive there might be besides good will.

## Why use Blockchain for this application?

  1. Taxes. Governments might use this to verify transactions. Smart contracts might send VAT directly to the government (aka split payment).
  
  2. Public resource publishing => innovation in search and derived services (e.g. route planning, logistics optimisation, etc). A big barrier to entry if you want to compete with Google Maps is data and this blockchain would provide that data.
  
  3. Search services can be paid for the actual service they provide, not advertising or selling user data. (Although I'm quite sure we'll find a way to throw advertising into the mix sooner or later.)
  
  4. Having this information available on a blockchain would supposedly provide a high-availability network for the smart-contract-backed transactions and for disseminating info to search services.
     
  5. If blockchain money ever really takes off this could minimise credit card fees and the like. (Who knows?)

## Open questions

Open architecture questions: who runs the blockchain nodes? Should it be public, private, permissioned? Is a general-purpose blockchain like Ethereum suitable? Could we run nodes in the infrastructure itself (EV charging stations, gas stations, cars, etc). What incentives would someone have to develop this blockchain? How can we maintain some degree of privacy for providers and consumers?

Open incentive-related questions: why would resource providers want to join this market? (E.g., what would make Uber or its drivers move away from its own platform.) Why would certification authorities join this blockchain?
