;;;; blockbender.asd

(asdf:defsystem #:blockbender
  :description "Describe blockbender here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :depends-on (:snooze :cl-who :hunchentoot :drakma :alexandria :cl-css
               :cl-fad :yason :st-json :parse-float)
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "deftemplate")
               (:file "blockbender")))
