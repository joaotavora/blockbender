;;;; blockbender.lisp

;; AIzaSyCj4cwZKKfDWifC2stjDBA6GDAh7DA_7Z8

(in-package #:blockbender)

(deftemplate:deftemplate with-basic-page
    (stream &key (title "Blockbender"))
  (:html
   (:head (:title (cl-who:str title))
          (:meta :name "viewport"
                 :content "width=device-width, initial-scale=1")
          (:link :rel "stylesheet"
                 :href "http://yui.yahooapis.com/pure/0.6.0/pure-min.css")
          (:link :href "/static/blockbender.css" :rel "stylesheet" :type "text/css")
          (:link :rel "shortcut icon" :href "/static/blockbender-small.png")
          ;; (:link :rel "stylesheet" :href "//code.jquery.com/ui/1.12.1/themes/vader/jquery-ui.css")
          (:script :src "https://code.jquery.com/jquery-1.12.4.js")
          ;; (:script :src "https://code.jquery.com/ui/1.12.1/jquery-ui.js")
          (:script :async "true" :defer "true" :src
                   "https://maps.googleapis.com/maps/api/js?key=AIzaSyCj4cwZKKfDWifC2stjDBA6GDAh7DA_7Z8&callback=initMap"
                   :type "text/javascript")
          (:script :src "/static/blockbender.js")
          (:style
           (str
            (cl-css:css (css)))))
   (:body
    (deftemplate:yield))))

(defun css ()
  `(
    ("@media screen and (max-width: 500px)"
     ("body" :padding 0px))
    ("@media screen and (max-width: 800px)"
     ("body" :padding 0px))
    ;; (section :height "100vh")
    ("#map" :position absolute :top 30px :bottom 0 :right 0 :left 0)
    ("*" :margin 0px :padding 0x)
    (header
     :padding-top 5px
     :display flex
     :flex-direction row
     :justify-content space-between)
    ("#message"
     :color tomato)
    (button
     :background-color "grey"
     :color white)
    (".dialog-holder"
     :padding-top 100px
     :display flex
     :flex-direction row
     :justify-content space-evenly
     :top 90px)
    (".dialog img"
     :width "100%")
    (".dialog"
     :border-radius 10px
     :border 3px
     :border-color darkslategrey
     :top 90px
     :width 200px
     :z-index 100
     :background "#f0f0ff")
    ("#splash "
     :width 400px)
    (".dialog-inner"
     :text-align center
     :padding 10px)
    ("#funds"
     :padding-left 20px)
    ("#restart-game"
     :margin-right 20px)))


(defparameter *weis-in-one-euro* 1000000) ;; one million
(defparameter *initial-funds* (* 500 *weis-in-one-euro*)) ;; in wei
(defparameter *fuel-capacity* 50000) ;; in milliliters
(defparameter *consumption* 5000/100) ;; in milliliters/km
(defparameter *average-price* 1000) ;; in weis/milliliter

(defun player-state-to-json ()
  (with-output-to-string (s)
    (st-json:write-json (st-json:jso
                         "funds"
                         (hunchentoot:session-value :funds)
                         "fuel"
                         (hunchentoot:session-value :fuel)
                         "full-p"
                         (if (= (hunchentoot:session-value :fuel) *fuel-capacity*)
                             :true
                             :false))
                        s)))

(defun reset-player-state ()
  (setf (hunchentoot:session-value :funds) *initial-funds*)
  (setf (hunchentoot:session-value :fuel) *fuel-capacity*)
  (setf (hunchentoot:session-value :wallet)
        (alexandria:random-elt *wallets*)))

(snooze:defroute home (:get :text/html &key)
  (reset-player-state)
  (with-output-to-string (s)
    (with-basic-page (s :title "Blockbender")
      (:header
       (:span :id "funds" "0")
       (:span :id "fuel" "0")
       (:span :id "message" "Message")
       (:button :id "restart-game" (str "Restart")))
      (:div :class "dialog-holder"
            (:div :id "splash" :class "dialog"
                  (:div :class "dialog-inner"
                        (:img :src "static/blockbender.png"))))
      (:div :class "dialog-holder"
            (:div :id "go-and-fill-dialog" :class "dialog"
                  (:div :class "dialog-inner"
                        (:p (:span :id "greeting" (esc "Welcome!")))
                        (:br)
                        (:p
                         (esc " Gas costs ")
                         (:span :id "price" (esc "YYY"))
                         (esc " wei/ml here."))
                        (:br)
                        (:button :id "go-and-fill" (str "Go there & Fill"))
                        )))
      (:div :id "map"))))

;; Matches GET contract.json
(defmethod st-json::write-json-element ((element real) stream)
  (format stream "~f" element))

(snooze:defroute contracts (:get :application/json north east south west)
  (with-output-to-string (s)
    (let* ((contracts (find-contracts :north-lat north
                                      :east-lng east
                                      :south-lat south
                                      :west-lng west)))
      (st-json:write-json
       (loop for contract in (subseq contracts 0 (min 200 (length contracts)))
             collect
             (apply #'st-json:jso (loop for (key val) on contract by #'cddr
                                        append (list (string-downcase key) val))))
       s))))

(snooze:defresource fill-er-up (method type &optional distance contract-address))
(snooze:defroute fill-er-up (:post :application/json
                                   &optional
                                   distance ;; in kilometers
                                   contract-address ;; address
                                   )
  (let* ((contract
           (find contract-address
                 *contracts*
                 :key (lambda (c) (getf c :address))
                 :test #'string-equal))
         (proper-contract-address
           (getf contract :address)))
    (unless contract
      (rip:http-condition 400))
    (when (> (* distance *consumption*)
             (hunchentoot:session-value :fuel))
      (rip:http-condition 404))
    (let ((fuel-spent (* distance *consumption*)))
      (decf (hunchentoot:session-value :fuel) fuel-spent)
      (let* ((price (getf contract :price))
             (cost-to-fill-up
               (*
                price
                (- *fuel-capacity*
                   (hunchentoot:session-value :fuel))))
             (money-to-spend
               (min cost-to-fill-up
                    (hunchentoot:session-value :funds)))
             (wallet
               (hunchentoot:session-value :wallet)))
        (decf (hunchentoot:session-value :funds)
              money-to-spend)
        (incf (hunchentoot:session-value :fuel)
              (/ money-to-spend price))
        ;; TCHA-TCHAM
        (sb-thread:make-thread
         (lambda ()
           (uiop:run-program
            (format nil "cd ~a && echo '{\"from\": \"~a\", \"contract\": \"~a\", \"value\": ~a}' | truffle exec scripts/make-deal.js"
                    (namestring (merge-pathnames
                                 #p"truffle/"
                                 (make-pathname :name nil
                                                :type nil
                                                :defaults (this-file))))
                    wallet
                    proper-contract-address
                    money-to-spend)
            :output t
            :error-output t))))))
  (player-state-to-json))

(snooze:defroute restart-game (:post :application/json)
  (reset-player-state)
  (player-state-to-json))

(setq snooze:*home-resource* #'home)
(setf (cl-who:html-mode) :html5)
(pushnew '("application/json" "json") hunchentoot::*mime-type-list* :test #'equal)
(setf (gethash "json" hunchentoot::*mime-type-hash*) "application/json")

(defvar *server* nil)

(defvar *all-cities* nil)

(defvar *contracts* nil)

(defvar *wallets* nil)

(defun find-contracts (&key north-lat east-lng south-lat west-lng)
  (remove-if-not (lambda (contract)
                   (and (<= west-lng (getf contract :lng) east-lng)
                        (<= south-lat (getf contract :lat) north-lat)))
                 *contracts*))

(defun this-file ()
  #.(or *compile-file-truename* *load-truename*))

(defun start (&optional port)
  ;; Read in cities
  (format t "Reading in cities.json~%")
  (force-output *standard-output*)
  (setq *all-cities*
        (with-open-file
            (s (make-pathname :name "static/cities" :type "json" :defaults (this-file)))
          (let ((yason:*parse-object-as* :plist)
                (yason:*parse-object-key-fn*
                  (lambda (str) (intern (string-upcase str) :keyword))))
            (yason:parse s))))
  (dolist (city *all-cities*)
    (setf (getf city :lat)
          (parse-float:parse-float (getf city :lat)))
    (setf (getf city :lng)
          (parse-float:parse-float (getf city :lng))))

  (setq *contracts*
        (with-open-file (s (make-pathname :name "capitals+contracts" :type "sexp" :defaults (this-file)))
          (loop for contract = (read s nil nil)
                for (city price address) = contract
                while contract
                append (loop for matching-city
                               in (remove-if-not (lambda (ac)
                                                   (string-equal (getf ac :name) city))
                                                 *all-cities*)
                             collect
                             (append
                              matching-city
                              `(:price ,price :address ,address))))))

  (setq *wallets*
        (with-open-file (s (make-pathname :name "wallets" :type "sexp" :defaults (this-file)))
          (read s)))

  (format t "Starting server~%")
  ;; Start server
  (when *server*
    (hunchentoot:stop *server*))
  (setq hunchentoot:*dispatch-table*
        (list (hunchentoot:create-folder-dispatcher-and-handler
               "/static/" (fad:merge-pathnames-as-directory (this-file)
                                                            "static/"))
              (snooze:make-hunchentoot-app)))
  (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port (or port 9003))))


