("Abu Dhabi"
 "Abuja"
 "Accra"
 "Adamstown, Pitcairn Island"
 "Addis Ababa"
 "Algiers"
 "Alofi"
 "Amman"
 "Amsterdam"
 "Ankara"
 "Antananarivo"
 "Apia"
 "Ashgabat"
 "Asmara"
 "Astana"
 "Asunción"
 "Athens"
 "Avarua"
 "Baghdad"
 "Baku"
 "Bamako"
 "Bandar Seri Begawan"
 "Bangkok"
 "Bangui"
 "Banjul"
 "Basseterre"
 "Beijing"
 "Beirut"
 "Belgrade"
 "Belmopan"
 "Berlin"
 "Bern"
 "Bishkek"
 "Bissau"
 "Bogotá"
 "Brasília"
 "Bratislava"
 "Brazzaville"
 "Bridgetown"
 "City of Brussels"
 "Bucharest"
 "Budapest"
 "Buenos Aires"
 "Bujumbura"
 "Cairo"
 "Canberra"
 "Caracas"
 "Castries"
 "Charlotte Amalie, United States Virgin Islands"
 "Chișinău"
 "Cockburn Town"
 "Conakry"
 "Copenhagen"
 "Dakar"
 "Damascus"
 "Dhaka"
 "Dili"
 "Djibouti (city)"
 "Dodoma"
 "Douglas, Isle of Man"
 "Dublin"
 "Dushanbe"
 "Edinburgh of the Seven Seas"
 "Flying Fish Cove"
 "Funafuti"
 "Gaborone"
 "George Town, Cayman Islands"
 "Georgetown, Ascension Island"
 "Gibraltar"
 "Guatemala City"
 "Gustavia, Saint Barthélemy"
 "Hagåtña"
 "Hamilton, Bermuda"
 "Hanga Roa"
 "Hanoi"
 "Harare"
 "Hargeisa"
 "Havana"
 "Helsinki"
 "Honiara"
 "Islamabad"
 "Jakarta"
 "Jamestown, Saint Helena"
 "Jerusalem"
 "Kabul"
 "Kampala"
 "Kathmandu"
 "Khartoum"
 "Kiev"
 "Kigali"
 "King Edward Point"
 "Kingston, Jamaica"
 "Kingston, Norfolk Island"
 "Kinshasa"
 "Kuala Lumpur"
 "Kuwait City"
 "Libreville"
 "Lilongwe"
 "Lima"
 "Lisbon"
 "Ljubljana"
 "Lomé"
 "London"
 "Luanda"
 "Lusaka"
 "Luxembourg (city)"
 "Madrid"
 "Majuro"
 "Malabo"
 "Malé"
 "Managua"
 "Manama"
 "Manila"
 "Maputo"
 "Marigot, Saint Martin"
 "Maseru"
 "Mata-Utu"
 "Mbabane"
 "Mexico City"
 "Minsk"
 "Mogadishu"
 "Monaco"
 "Monrovia"
 "Montevideo"
 "Moroni, Comoros"
 "Moscow"
 "Muscat, Oman"
 "Nairobi"
 "Nassau, Bahamas"
 "Naypyidaw"
 "N'Djamena"
 "New Delhi"
 "Ngerulmud"
 "Niamey"
 "Nicosia"
 "Nicosia"
 "Nouakchott"
 "Nouméa"
 "Nukuʻalofa"
 "Nuuk"
 "Ottawa"
 "Ouagadougou"
 "Pago Pago"
 "Palikir"
 "Panama City"
 "Papeete"
 "Paramaribo"
 "Paris"
 "Philipsburg, Sint Maarten"
 "Plymouth, Montserrat"
 "Podgorica"
 "Port Louis"
 "Port Moresby"
 "Port Vila"
 "Port-au-Prince"
 "Port of Spain"
 "Porto-Novo"
 "Prague"
 "Praia"
 "Pretoria"
 "Pristina"
 "Quito"
 "Rabat"
 "Reykjavík"
 "Riga"
 "Riyadh"
 "Road Town"
 "Rome"
 "Roseau"
 "Saipan"
 "San José, Costa Rica"
 "San Juan, Puerto Rico"
 "City of San Marino"
 "San Salvador"
 "Sana'a"
 "Santiago"
 "Santo Domingo"
 "São Tomé"
 "Sarajevo"
 "Seoul"
 "Singapore"
 "Skopje"
 "Sofia"
 "Sri Jayawardenepura Kotte"
 "St. George's, Grenada"
 "St. Helier"
 "St. John's, Antigua and Barbuda"
 "St. Peter Port"
 "St. Pierre, Saint-Pierre and Miquelon"
 "Stanley, Falkland Islands"
 "Stepanakert"
 "Sucre"
 "Taipei"
 "Tarawa"
 "Tashkent"
 "Tbilisi"
 "Tegucigalpa"
 "Tehran"
 "Thimphu"
 "Tirana"
 "Tiraspol"
 "Tórshavn"
 "Tskhinvali"
 "Ulaanbaatar"
 "Vaduz"
 "Valletta"
 "The Valley, Anguilla"
 "Vatican City"
 "Victoria, Seychelles"
 "Vienna"
 "Vientiane"
 "Vilnius"
 "Warsaw"
 "Washington, D.C."
 "Wellington"
 "West Island, Cocos (Keeling) Islands"
 "Yamoussoukro"
 "Yaoundé"
 "Yaren district"
 "Zagreb")
