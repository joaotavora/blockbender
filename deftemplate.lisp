(defpackage #:deftemplate
  (:use #:cl #:cl-who)
  (:export
   #:deftemplate
   #:yield))
(in-package :deftemplate)

(setq cl-who:*prologue* "<!DOCTYPE html>")

(defmacro deftemplate (name (stream-var &rest lambda-list-args) &body who-args)
  "A micro HTML-templating framework"
  (alexandria:with-gensyms (body function)
    `(progn
       (setf (get ',name 'deftemplate)
             (lambda (,function ,stream-var ,@lambda-list-args)
               (macrolet ((yield () '(funcall ,function)))
                 (with-html-output (,stream-var nil :indent t :prologue t)
                   ,@who-args)
                 nil)))

       (defmacro ,name (&whole args (stream ,@lambda-list-args) &body ,body)
         (declare (ignore ,@(loop for thing in lambda-list-args
                                  for sym = (if (consp thing) (car thing) thing)
                                  unless (eq #\& (char (string sym) 0))
                                    collect sym)))
         `(apply (get ',',name 'deftemplate)
                 (lambda ()
                   (with-html-output (,stream nil :indent t)
                     ,@,body))
                 ,stream
                 ,(cons 'list (rest (second args))))))))


