# Blockchain Setup

https://arctouch.com/blog/how-to-set-up-ethereum-blockchain/

Some of these commands are in blockhain/*.sh for future convenience.

## Setting up the server/node/whatever

    $ sudo add-apt-repository -y ppa:ethereum/ethereum
    $ sudo apt-get update
    $ sudo apt-get install ethereum


## Authority account

This account can seal others.
    
    $ geth --datadir blockbender/ account new
    INFO [11-17|11:43:58.860] Maximum peer count                       ETH=25 LES=0 total=25
    Passphrase: blockbender
    Address: {4a1d59a3fe785868d704c1913e8ff1edf004b0fd}
    

## Prefunded account (aka The Central Bank)

    $ geth --datadir blockbender/ account new
    INFO [11-17|12:27:15.042] Maximum peer count                       ETH=25 LES=0 total=25
    Passphrase: blockbender
    Address: {7af222dad7a8f65816906a141570c06b4ca8f0d5}
    

## Creating the genesis block

    $ puppeth
    
    Please specify a network name to administer (no spaces or hyphens, please)
    > blockbender
    
    Sweet, you can set this via --network=blockbender next time!
    
    INFO [11-17|12:29:58.067] Administering Ethereum network           name=blockbender
    
    Which consensus engine to use? (default = clique)
     1. Ethash - proof-of-work
     2. Clique - proof-of-authority
    > 2
    
    How many seconds should blocks take? (default = 15)
    > 0.5
    ERROR[11-17|12:36:50.017] Invalid input, expected integer          err="strconv.Atoi: parsing \"0.5\": invalid syntax"
    > 1
    
    Which accounts are allowed to seal? (mandatory at least one)
    > 0x4a1d59a3fe785868d704c1913e8ff1edf004b0fd
    
    Which accounts should be pre-funded? (advisable at least one)
    > 0x7af222dad7a8f65816906a141570c06b4ca8f0d5
    
    Specify your chain/network ID if you want an explicit one (default = random)
    > 42
    INFO [11-17|12:38:21.031] Configured new genesis block


## Importing/initialising the genesis block

    $ geth --datadir datadir init genesis.json
    INFO [11-17|13:48:52.939] Maximum peer count                       ETH=25 LES=0 total=25
    INFO [11-17|13:48:52.939] Allocated cache and file handles         database=/home/luis/src/blockbender/blockchain/datadir/geth/chaindata cache=16 handles=16
    INFO [11-17|13:48:52.956] Writing custom genesis block 
    INFO [11-17|13:48:52.976] Persisted trie from memory database      nodes=356 size=51.99kB time=2.698916ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
    INFO [11-17|13:48:52.977] Successfully wrote genesis state         database=chaindata                                                    hash=a1f66f…2bb6e6
    INFO [11-17|13:48:52.977] Allocated cache and file handles         database=/home/luis/src/blockbender/blockchain/datadir/geth/lightchaindata cache=16 handles=16
    INFO [11-17|13:48:52.991] Writing custom genesis block 
    INFO [11-17|13:48:53.016] Persisted trie from memory database      nodes=356 size=51.99kB time=5.034531ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
    INFO [11-17|13:48:53.016] Successfully wrote genesis state         database=lightchaindata                                                    hash=a1f66f…2bb6e6
    

## Launching the root miner

TODO: figure out how to avoid mining 0-transaction
blocks. [revelant Ethereum SX question](https://ethereum.stackexchange.com/questions/3151/how-to-make-miner-to-mine-only-when-there-are-pending-transactions)

    $ geth --nodiscover --networkid 42 --datadir datadir --unlock 0x4a1d59a3fe785868d704c1913e8ff1edf004b0fd --mine --rpc --rpcapi eth,net,web3 --rpcaddr 127.0.0.1
