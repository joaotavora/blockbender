# Goals for the hackathon

A minimal proof-of-concept could be:

  1. A simple Smart Contract template for publishing resources. Let's
     skip certification. Let's use the test Ethereum blockchain (or a
     local one, whichever's easiest). Let's a pick a simple resource
     provider to reason about like gas stations.

  2. A server monitors the blockchain and indexes available resources.

  3. A simple web app displays available resources on a map.

  4. Users can activate the contract (possibly using the same web app,
     for simplicity) which would perform the
     transaction/validation. E.g.: the pump station can have a QR code
     (in real life, things like Via Verde could take care of
     communicating with the pump) that the consumer can use to
     activate the smart contract. After pumping is done
     (implementation: dummy progress bar :-)), the pump activates the
     smart contract and consumer's money is automatically transfered
     to resource and search providers.
