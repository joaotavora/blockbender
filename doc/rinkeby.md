Tutorial: https://gist.github.com/cryptogoth/10a98e8078cfd69f7ca892ddbdcf26bc

Set up a light node: https://www.rinkeby.io/#geth and created a new wallet:

    geth --rinkeby attach ~/.ethereum/rinkeby/geth.ipc console

    > personal.newAccount("blockbender")
    "0xffe4d76fd81e1e5992f9c5eaa112e7b631e64a1d"

[Tweeted](https://twitter.com/luismbo_eo/status/1063840043062751232), [got 18.75 ETH](https://rinkeby.etherscan.io/tx/0x8136b9e7e578c1e57e70dd8bb98e05d0aef7ffa14c4c51a677a720462e804f0c).

