var globalMap;
var globalMarkers = [];
var globalCountries = [];
var player = null;
var globalUpdateTimer = null;
var globalDebug = null;
var initialContract = {
    lat: 38.7223,
    lng: -9.1393,
    name: "Lisbon",
    country: "PT"
};

function getCountryByCode(code) {
    return globalCountries.filter(
        function(data){ return data.code == code }
    );
}

function getIconByPrice(price) {
    if (price < 800) {
        return "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
    } else if (price < 1100) {
        return "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png";
    } else  {
        return "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    };
}

function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
	return 0;
    }
    else {
	var radlat1 = Math.PI * lat1/180;
	var radlat2 = Math.PI * lat2/180;
	var theta = lon1-lon2;
	var radtheta = Math.PI * theta/180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	if (dist > 1) {
	    dist = 1;
	}
	dist = Math.acos(dist);
	dist = dist * 180/Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist;
    }
}

function afterMapsLoaded() {
    $("#restart-game").click();
}

function initMap() {
    var location = {
        lat: 38.7223,
        lng: -9.1393
    };
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 6,
        center: location,
        disableDefaultUI: true,
        mapTypeId: 'terrain'
    });

    globalMap = map;


    player = new google.maps.Marker({
        position: location,
        map: globalMap,
        label: "b",
        icon: {
            url: "static/blockbender-small.png"
        }
    });

    google.maps.event.addListenerOnce(map, 'tilesloaded', afterMapsLoaded);
    map.addListener('idle',function() { getContracts(); });
    map.addListener('click',function() { $(".dialog").hide(); });
}

function updateState(state) {
    $("#fuel").html(`${(state["fuel"]/1000).toFixed(1)} liters left!`);
    $("#funds").html(`${state["funds"]} wei`);
    if (state["full-p"]) {
        $("#funds").show();
        $("#fuel").hide();
    } else {
        $("#fuel").show();
        $("#funds").hide();
    }
}

function getContracts() {
    var bounds = globalMap.getBounds();
    var northeast = bounds.getNorthEast();
    var southwest = bounds.getSouthWest();

    $.getJSON('contracts.json' +
              '/' + northeast.lat() +
              '/' + northeast.lng() +
              '/' + southwest.lat() +
              '/' + southwest.lng()
              , function(data) {
                  $.each(globalMarkers, function(i, oldMarker) {
                      oldMarker.setMap(null); // remove it
                  });
                  $.each(data, function(i, contract) {
                      var marker = new google.maps.Marker({
                          position: new google.maps.LatLng(contract["lat"],
                                                           contract["lng"]),
                          map: globalMap,
                          icon: {
                              url: getIconByPrice(contract["price"])
                          }
                      });
                      globalMarkers.push(marker);
                      marker.addListener('click', function() {
                          var city = contract["name"];
                          var countryName = getCountryByCode(contract["country"])[0]["name"];

                          $("#go-and-fill-dialog").data().contract = contract;
                          
                          $("#go-and-fill-dialog #greeting").html(
                              `Welcome to ${city}, ${countryName}`
                          );
                          $("#go-and-fill-dialog #price").html(contract["price"]);
                          $("#go-and-fill-dialog").show();
                      });
                  });
              });
};


$(document).ready(function() {
    $("#go-and-fill-dialog").hide();
    $("#splash").hide();
    $("#splash").on("click",function() {
        $("#splash").hide();
    })

    $.getJSON('static/countries.json').done(function(data) {
        globalCountries = data;
    });
    // $("#go-and-fill-dialog").dialog({
    //     autoOpen: true,
    //     show: {
    //         effect: "fade",
    //         duration: 200
    //     },
    //     hide: {
    //         effect: "fade",
    //         duration: 200
    //     }
    // });

    $("#update").on("click", function() {
        getContracts();
    });

    function goAndFill (contract) {
        var targetPosition = [contract["lat"],contract["lng"]];
        var currentPosition = [player.position.lat(),player.position.lng()];

        var numDeltas = 100;
        var delay = 10;

        var deltaLat = (targetPosition[0] - currentPosition[0])/numDeltas;
        var deltaLng = (targetPosition[1] - currentPosition[1])/numDeltas;

        var i = 0;
        moveIncrement();

        function moveIncrement(){
            currentPosition[0] += deltaLat;
            currentPosition[1] += deltaLng;
            var latlng = new google.maps.LatLng(currentPosition[0], currentPosition[1]);
            player.setPosition(latlng);
            if(i!=numDeltas){
                i++;
                setTimeout(moveIncrement, delay);
            } else {
                globalMap.setCenter(player.position);
                $("#message").html(`Arrived in ${contract["name"]}!`);
                $("#message").css('color','black');
            }
        }
    }

    $("#cancel").on("click", function () { $("#go-and-fill-dialog").hide(); });
    
    $("#go-and-fill").on("click", function () {
        var contract = $("#go-and-fill-dialog").data().contract;

        var dist = distance(player.position.lat(),player.position.lng(),
                            contract["lat"],contract["lng"]);
        var jqxhr = $.post("fill-er-up.json/" + dist + "/" + contract["address"])
            .done(function(data) {
                updateState($.parseJSON(data));
                goAndFill(contract);
            }).fail(function(data) {
                $("#message").html(`Not enough fuel to reach ${contract["name"]}!`);
                $("#message").css('color','tomato');
            }).always(function() {
                $("#go-and-fill-dialog").hide();
            })
    });

    $("#restart-game").on("click", function() {
        var jqxhr = $.post("restart-game.json")
            .done(function(data) {
                updateState($.parseJSON(data));
                goAndFill(initialContract);
            }).fail(function(data) {
                globalDebug=data;
                alert( "serious error, sorry " + data);
            }).always(function() {
                $("#go-and-fill-dialog").hide();
            })
    });
});
