pragma solidity ^0.4.7;

contract Advert {

    /* Advert info */
    string public advertDescription;
    struct Position {
        int latitude; // * 10000
        int longitude; // * 10000
    }
    Position position;
    uint public pricePerLiter; // change to ml
    uint public litersSold = 0;

    /* Deal info */
    struct DealInfo {
        address clientId;
        uint DepositedFuel;
    }

    DealInfo deal;

    //mapping(address => DealInfo[]) public dealsList;
    DealInfo[] public dealsList;

     /* This is the constructor which will be called once when the contract is deployed to the blockchain.   */
    constructor(string new_advertDescription, int new_latitude, int new_longitude, uint new_pricePerLiter) public {
        advertDescription = new_advertDescription;
        position.latitude = new_latitude;
        position.longitude = new_longitude;
        pricePerLiter = new_pricePerLiter;
    }

    event Paid(address _from, uint _value);

    function Deal() payable {
        uint fuel = msg.value / pricePerLiter;
        deal.clientId = msg.sender;
        deal.DepositedFuel = fuel;
        Advert.litersSold += fuel;
        dealsList.push(deal);
        emit Paid(msg.sender, msg.value);
    }

    function getClientId() public constant returns (address) {
        return deal.clientId;
    }

    function getDepositedFuel() public constant returns (uint) {
        return deal.DepositedFuel;
    }

    function getDealsListLength() public constant returns (uint) {
        return dealsList.length;
    }
}
