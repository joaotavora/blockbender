var Advert = artifacts.require("./Advert.sol");
const fs = require('fs');

let rawdata = fs.readFileSync('capitals.json');
let capitals = JSON.parse(rawdata);

module.exports = function(deployer) {
    var output = "";
    for (var i = 0; i < capitals.length; i++) {
        (function (x) {
            var cost = Math.floor(Math.random() * 1600 + 400);
            var city = capitals[x];
            deployer.deploy(Advert, `City ${city}`, 0, 0, cost).then(
                function() {
                    var contract = Advert.address;
                    console.log(`Deployed ${city} (${cost} wei/ml): ${contract}`);
                    output += `("${city}" ${cost} "${contract}")\n`;
                    if (x+1 == capitals.length) {
                        console.log("Writing final output");
                        var stream = fs.createWriteStream("capitals+contracts.sexp");
                        stream.once('open', function(fd) {
                            stream.write(output);
                            stream.end();
                        });
                    }
                });
        })(i);
    }
};
