const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

module.exports = function(callback) {
    var Advert = artifacts.require("./Advert.sol");

    rl.question('', (answer) => {
        console.log(`Got: ${answer}`);
        rl.close();
        var contract = answer;

        var meta;
        Advert.at(contract).then(function(instance) {
            meta = instance;
            return meta.getDepositedFuel.call({from: "0xE160430Af73ED40A366Df502633cC82259f56009"});
        }).then(function(fuel) {
            console.log(`Fuel: ${fuel}`);
            callback();
        }).catch(function(e) {
            console.log('Error:' + e);
        })
    });
}
