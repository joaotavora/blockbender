// Example run:
// echo '{"from": "0xE160430Af73ED40A366Df502633cC82259f56009", "contract": "0x74eb5736d1c14e3fe0f013e26ade4f7458cab9fd", "value": 123}' | truffle exec scripts/make-deal.js

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

module.exports = function(callback) {
    var Advert = artifacts.require("./Advert.sol");

    rl.question('', (answer) => {
        console.log(`Got: ${answer}`);
        rl.close();
        var info = JSON.parse(answer);

        var meta;
        Advert.at(info.contract).then(function(instance) {
            meta = instance;
            return meta.Deal({from: info.from, value: info.value});
        }).then(function(result) {
            console.log(`Made deal: ${info}`);
            callback();
        }).catch(function(e) {
            console.log('Error:' + e);
        })
    });
}
