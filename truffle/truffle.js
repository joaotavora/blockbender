module.exports = {
    networks: {
        development: {
            host: "10.1.3.220",
            port: 7545,
            network_id: "*" // Match any network id
        }
    }
};
